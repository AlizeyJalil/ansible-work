#! /bin/bash

JAVA_HOME=$(readlink -f /usr/bin/java | sed "s:/jre/bin/java::")

export JAVA_OPTS="$JAVA_OPTS -Xms25m -Xmx50m -XX:MaxPermSize=50m"
