#! /bin/bash

JAVA_HOME=$(readlink -f /usr/bin/java | sed "s:/jre/bin/java::")

export CATALINA_OPTS="$CATALINA_OPTS -Xms64m"
export CATALINA_OPTS="$CATALINA_OPTS -Xmx128m"
export CATALINA_OPTS="$CATALINA_OPTS -XX:MaxPermSize=128m"
